SHELL='/bin/bash'

composer-install-dev:
	make custom-composer-docker-run COMMAND="install"

test-unit:
	make unit-test-docker-run RUNPATH="$(FILE)"

composer-build:
	@docker build \
			--quiet \
			--tag custom-composer \
			make/composer

docker-build-test:
	@docker build \
			--quiet \
			--tag unit-test \
			make/test

custom-composer-docker-run:
	make composer-build
	@docker run \
			--rm \
			--tty \
			--interactive \
			--volume "$(PWD)":/app \
			--user $(shell id -u):$(shell id -g) \
			custom-composer ${COMMAND}

unit-test-docker-run:
	make composer-install-dev
	make docker-build-test
	@docker run \
			--rm \
			--tty \
			--interactive \
			--volume "$(PWD)":/app \
            --volume "$(PWD)/make/test/php.ini":/usr/local/etc/php/conf.d/custom.ini \
            --workdir /app \
			unit-test bin/phpunit ${RUNPATH}