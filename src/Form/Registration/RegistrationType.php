<?php

declare(strict_types=1);

namespace App\Form\Registration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

final class RegistrationType extends AbstractType
{
    const USERNAME = 'username';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const CONFIRM_PASSWORD = 'confirmPassword';
    const REGISTER = 'register';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod('POST')
            ->add(self::USERNAME, TextType::class)
            ->add(self::EMAIL, EmailType::class)
            ->add(self::PASSWORD, PasswordType::class)
            ->add(self::CONFIRM_PASSWORD, PasswordType::class)
            ->add(self::REGISTER, SubmitType::class);
    }
}
