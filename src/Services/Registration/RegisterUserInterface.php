<?php

declare(strict_types=1);

namespace App\Services\Registration;

use App\Entity\Registration\RegistrationFormDataInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface RegisterUserInterface
{
    public function register(RegistrationFormDataInterface $formData): ResponseInterface;
}
