<?php

declare(strict_types=1);

namespace App\Services\Registration;

use App\Entity\Registration\RegistrationFormDataInterface;
use App\Form\Registration\RegistrationType;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RegisterUser implements RegisterUserInterface
{
    /** @throws TransportExceptionInterface */
    public function register(RegistrationFormDataInterface $formData): ResponseInterface
    {
        $client = HttpClient::create();
        return $client->request(
            'POST',
            'http://registration/public/register',
            [
                'query' => [
                    RegistrationType::USERNAME => $formData->getUsername(),
                    RegistrationType::EMAIL => $formData->getEmail(),
                    RegistrationType::PASSWORD => $formData->getPassword(),
                    RegistrationType::CONFIRM_PASSWORD => $formData->getConfirmPassword(),
                ]
            ]
        );
    }
}