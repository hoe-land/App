<?php

declare(strict_types=1);

namespace App\Entity\Registration;

interface RegistrationFormDataInterface
{
    public function setUsername(string $username): self;
    public function setEmail(string $email): self;
    public function setPassword(string $password): self;
    public function setConfirmPassword(string $confirmPassword): self;

    public function getUsername(): string;
    public function getEmail(): string;
    public function getPassword(): string;
    public function getConfirmPassword(): string;
}
