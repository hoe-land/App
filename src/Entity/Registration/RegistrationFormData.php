<?php

declare(strict_types=1);

namespace App\Entity\Registration;

final class RegistrationFormData implements RegistrationFormDataInterface
{
    private $username;
    private $email;
    private $password;
    private $confirmPassword;

    public function __construct()
    {
        $this->username = '';
        $this->email = '';
        $this-> password = '';
        $this->confirmPassword = '';
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function setConfirmPassword(string $confirmPassword): self
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getConfirmPassword(): string
    {
        return $this->confirmPassword;
    }
}
