<?php

declare(strict_types=1);

namespace App\Entity\TemplateData;

final class TemplateData implements TemplateDataInterface
{
    private $path;
    private $params;

    public function __construct(string $path, array $params = [])
    {
        $this->path = $path;
        $this->params = $params;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}
