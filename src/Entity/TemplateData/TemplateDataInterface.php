<?php

declare(strict_types=1);

namespace App\Entity\TemplateData;

interface TemplateDataInterface
{
    public function getPath(): string;
    public function getParams(): array;
}
