<?php

declare(strict_types=1);

namespace App\Repository\Registration;

interface TemplatePathsInterface
{
    public function getFormPath(): string;
    public function getSuccessPath(): string;
    public function getFailurePath(): string;
}
