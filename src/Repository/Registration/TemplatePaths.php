<?php

declare(strict_types=1);

namespace App\Repository\Registration;

final class TemplatePaths implements TemplatePathsInterface
{
    private const FORM_TEMPLATE_PATH = 'register.html.twig';
    private const SUCCESS_TEMPLATE_PATH = 'registration_success.html.twig';
    private const FAILURE_TEMPLATE_PATH = 'registration_failure.html.twig';

    public function getFormPath(): string
    {
        return self::FORM_TEMPLATE_PATH;
    }

    public function getSuccessPath(): string
    {
        return self::SUCCESS_TEMPLATE_PATH;
    }

    public function getFailurePath(): string
    {
        return self::FAILURE_TEMPLATE_PATH;
    }
}
