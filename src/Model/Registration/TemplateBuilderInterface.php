<?php

declare(strict_types=1);

namespace App\Model\Registration;

use App\Entity\TemplateData\TemplateData;
use Symfony\Component\HttpFoundation\Request;

interface TemplateBuilderInterface
{
    public function getTemplateData(Request $request): TemplateData;
}
