<?php

declare(strict_types=1);

namespace App\Model\Registration;

use App\Entity\TemplateData\TemplateData;
use App\Repository\Registration\TemplatePathsInterface;
use Symfony\Component\HttpFoundation\Request;

final class TemplateBuilder implements TemplateBuilderInterface
{
    private $templatePaths;

    public function __construct(TemplatePathsInterface $templatePaths)
    {
        $this->templatePaths = $templatePaths;
    }

    public function getTemplateData(Request $request): TemplateData
    {
        return new TemplateData('',[]);
    }
}
