<?php

namespace App\Controller;

use App\Entity\Registration\RegistrationFormData;
use App\Form\Registration\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class RegistrationController extends AbstractController
{
    const VALID_REGISTRATION = 'validRegistration';

    /**
     * @Route("/register", name="register")
     */
    public function handle(Request $request)
    {
        $form = $this->createForm(RegistrationType::class, new RegistrationFormData());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->handleSubmittedForm($form);
        }

        return $this->render(
            'register.html.twig',
            [
                'registrationForm' => $form->createView(),
            ]
        );
    }

    private function handleSubmittedForm(FormInterface $form): RedirectResponse
    {
        try {
            $response = $this->registerUser($form);

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                return $this->failureRedirect();
            }
        } catch (TransportExceptionInterface $exception) {
            return $this->failureRedirect();
        }

        return $this->successRedirect($form);
    }

    /** @throws TransportExceptionInterface */
    private function registerUser(FormInterface $form): ResponseInterface
    {
        $client = HttpClient::create();
        return $client->request(
            'POST',
            'http://registration/public/register',
            [
                'query' => [
                    RegistrationType::USERNAME => $form->get(RegistrationType::USERNAME)->getData(),
                    RegistrationType::EMAIL => $form->get(RegistrationType::EMAIL)->getData(),
                    RegistrationType::PASSWORD => $form->get(RegistrationType::PASSWORD)->getData(),
                    RegistrationType::CONFIRM_PASSWORD => $form->get(RegistrationType::CONFIRM_PASSWORD)->getData(),
                ]
            ]
        );
    }

    private function successRedirect(FormInterface $form): RedirectResponse
    {
        $this->addFlash(self::VALID_REGISTRATION, true);
        $this->addFlash('userEmail', $form->get(RegistrationType::EMAIL)->getData());
        return $this->redirectToRoute('registration-success');
    }

    private function failureRedirect(): RedirectResponse
    {
        $this->addFlash(self::VALID_REGISTRATION, true);
        return $this->redirectToRoute('registration-failure');
    }
}
