<?php

declare(strict_types=1);

namespace App\Repository\Registration;

use PHPUnit\Framework\TestCase;

final class TemplatePathsTest extends TestCase
{
    private $sut;

    public function setUp(): void
    {
        $this->sut = new TemplatePaths();
    }

    public function testGetFailurePath()
    {
        $expected = 'registration_failure.html.twig';
        $actual = $this->sut->getFailurePath();
        $this->assertSame($expected, $actual);
    }

    public function testGetSuccessPath()
    {
        $expected = 'registration_success.html.twig';
        $actual = $this->sut->getSuccessPath();
        $this->assertSame($expected, $actual);
    }

    public function testGetFormPath()
    {
        $expected = 'register.html.twig';
        $actual = $this->sut->getFormPath();
        $this->assertSame($expected, $actual);
    }
}
