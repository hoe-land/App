<?php

declare(strict_types=1);

namespace App\Services\Registration;

use App\Entity\Registration\RegistrationFormDataInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class RegisterUserTest extends TestCase
{
    /** @dataProvider dataProvider */
    public function testRegisterUser(int $expected, array $userData)
    {
        $formDataMock = Mockery::mock(RegistrationFormDataInterface::class);

        $formDataMock
            ->shouldReceive('getUsername')
            ->once()
            ->withNoArgs()
            ->andReturn($userData['username']);
        $formDataMock
            ->shouldReceive('getPassword')
            ->once()
            ->withNoArgs()
            ->andReturn($userData['password']);
        $formDataMock
            ->shouldReceive('getConfirmPassword')
            ->once()
            ->withNoArgs()
            ->andReturn($userData['confirm_password']);
        $formDataMock
            ->shouldReceive('getEmail')
            ->once()
            ->withNoArgs()
            ->andReturn($userData['email']);

        $sut = new RegisterUser();
        $actual = $sut->register($formDataMock);
        $this->assertSame($expected, $actual);
    }

    public function dataProvider(): array
    {
        return [
            'valid' => [
                'expected' => Response::HTTP_OK,
                'userData' => [
                    'username' => '',
                    'password' => '',
                    'confirm_password' => '',
                    'email' => '',
                ],
            ],
        ];
    }
}
