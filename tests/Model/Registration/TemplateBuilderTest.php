<?php

declare(strict_types=1);

namespace App\Model\Registration;

use App\Entity\TemplateData\TemplateDataInterface;
use App\Repository\Registration\TemplatePathsInterface;
use Mockery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

final class TemplateBuilderTest extends TestCase
{
    /** @dataProvider dataProvider */
    public function testGetTemplateData(
        string $templatePath,
        array $templateParams,
        Request $request
    ) {
        $templatePathsMock = Mockery::mock(TemplatePathsInterface::class);
        $sut = new TemplateBuilder($templatePathsMock);

        $actual = $sut->getTemplateData($request);

        $this->assertSame($templatePath, $actual->getPath());
        $this->assertSame($templateParams, $actual->getParams());
    }

    public function dataProvider(): array
    {
        return [
            'form' => [
                'templatePath' => 'register.html.twig',
                'templateParams' => [],
                'request' => new Request(),
            ],
            'success' => [
                'templatePath' => 'registration_failure.html.twig',
                'templateParams' => [
                    'userEmail' => '',
                ],
                'request' => new Request(),
            ],
            'failure' => [
                'templatePath' => 'registration_success.html.twig',
                'templateParams' => [],
                'request' => new Request(),
            ],
        ];
    }
}
