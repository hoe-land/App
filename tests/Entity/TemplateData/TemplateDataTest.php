<?php

declare(strict_types=1);

namespace App\Entity\TemplateData;

use PHPUnit\Framework\TestCase;

class TemplateDataTest extends TestCase
{
    const PATH = 'path';
    const PARAMS = 'params';

    /** @dataProvider dataProvider */
    public function testGetParams(string $expectedPath, array $expectedParams)
    {
        $sut = new TemplateData($expectedPath,$expectedParams);
        $actual = $sut->getParams();
        $this->assertSame($expectedParams, $actual);
    }

    /** @dataProvider dataProvider */
    public function testGetPath(string $expectedPath, array $expectedParams)
    {
        $sut = new TemplateData($expectedPath,$expectedParams);
        $actual = $sut->getPath();
        $this->assertSame($expectedPath, $actual);
    }
    public function dataProvider(): array
    {
        return [
            'full' => [
                self::PATH => 'test_template.html.twig',
                self::PARAMS => [
                    'param_1_key' => 'param_1_value',
                    'param_2_key' => 'param_2_value',
                ],
            ],
            'missingParams' => [
                self::PATH => 'test_template.html.twig',
                self::PARAMS => [],
            ],
        ];
    }
}
